package com.example.app;

import com.example.dep.Dep;

public class App 
{
    public static void main( String[] args )
    {
        System.out.println("Welcome Gitlab POC");
        Dep.hello( "GitLab" );
        System.out.println("End");
    }
}
